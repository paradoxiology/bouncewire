Project Overview
================

The main purpose of this project is to allow me to get familiar with Netwire 5 and the FRP(Functional Reactive Programming) paradigm.

The program will periodically spawn balls out of thin air. Each ball has a fixed lifetime before it disappears. Alternatively, user is able to spawn a ball by pressing the **space** key.

Balls will bounce around, but they don't collide with each other.

You can alter the world's gravity by hitting the **arrow** keys.

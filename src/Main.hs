{-# LANGUAGE Arrows #-}

import Control.Wire hiding (Event)
import qualified Control.Wire as NW (Event)

import Control.Monad.Random
import Control.Monad.Reader (ReaderT, runReaderT, asks)

import Data.Vect

import Graphics.Gloss
import Graphics.Gloss.Interface.Pure.Game

import Prelude hiding ((.), id)

-- import Debug.Trace

import WireUtils


main :: IO ()
main = play 
         (InWindow "Glossy Wire" (600, 480) (600, 10))
         black
         100
         initWorld
         render
         handleEvents
         update

data CollisionType = HorizontalCollision
                   | VerticalCollision
                   | NoCollision

data WorldEnv = WorldEnv
  { worldWidth  :: !Float
  , worldHeight :: !Float
  } deriving (Show)

-- | G Monad captures the game environment and the Random monad 
-- used in the wires
type G = RandT StdGen (ReaderT WorldEnv Identity)

runG :: G a -> StdGen -> WorldEnv -> (a, StdGen)
runG m g env = runIdentity (runReaderT (runRandT m g) env)

type GlossWire a b = Wire (Timed Float ()) () G a b

data WireWorld = WireWorld
  { wire          :: GlossWire [Event] [Ball]
  , balls         :: [Ball]
  , pendingEvents :: [Event]
  , randGen       :: !StdGen -- Without the bang, the program will space leak
  , worldEnv      :: WorldEnv
  }

data Ball = Ball !Vec2 !Color deriving (Show)

initWorld :: WireWorld
initWorld = WireWorld
              animateBalls
              []
              []
              (mkStdGen 8552)
              (WorldEnv 600 480)

handleEvents :: Event -> WireWorld -> WireWorld
handleEvents (EventResize (newWidth, newHeight)) oldWorld = 
  oldWorld{worldEnv = WorldEnv (fromIntegral newWidth) (fromIntegral newHeight)}

handleEvents event@(EventKey key state _ _) oldWorld@(WireWorld{pendingEvents = events}) =
  case state of
    Up -> oldWorld{pendingEvents = event : deleteKeyEvent key events}
            where deleteKeyEvent k = filter $ \(EventKey k' _ _ _) -> k' /= k

    _  -> oldWorld{pendingEvents = event:events}

handleEvents _ w = w

deleteUpKeyEvents :: [Event] -> [Event]
deleteUpKeyEvents = filter $ \(EventKey _ keyState _ _) -> keyState /= Up

render :: WireWorld -> Picture
render world = pictures $ map mkCircle (balls world)

update :: Float -> WireWorld -> WireWorld
update dt oldWorld = WireWorld w' bs' (deleteUpKeyEvents events) g' env
  where
    events = pendingEvents oldWorld
    env = worldEnv oldWorld
    ((ebs, w'), g') = runG
                        (stepWire (wire oldWorld) (Timed dt ()) (Right events))
                        (randGen oldWorld)
                        env
    bs' = either (const []) id ebs

mkCircle :: Ball -> Picture
mkCircle (Ball (Vec2 x y) c) = Translate x y $ color c $ circleSolid 10

isKeyActive :: KeyState -> SpecialKey -> [Event] -> Bool
isKeyActive keyState k  = any checkKeyActive
  where 
    checkKeyActive (EventKey (SpecialKey sk) state _ _) = state == keyState && sk == k
    checkKeyActive _                                    = False

isKeyDown :: SpecialKey -> [Event] -> Bool
isKeyDown = isKeyActive Down

isKeyUp :: SpecialKey -> [Event] -> Bool
isKeyUp = isKeyActive Up

animateBalls :: GlossWire [Event] [Ball]
animateBalls = proc events -> do
  newDropBall <- periodicSpawnBall -< ()
  newUserBall <- userSpawnBall -< events
  let newBalls = concatE newDropBall newUserBall
  balls' <- accumWiresE -< (events, newBalls)
  returnA  -< balls'

userSpawnBall :: GlossWire [Event] (NW.Event (GlossWire [Event] Ball))
userSpawnBall = onThenE (isKeyUp KeySpace) (for 10 . randomBall)

periodicSpawnBall :: GlossWire a (NW.Event (GlossWire [Event] Ball))
periodicSpawnBall = periodic 2 . pure (for 10 . randomBall)

randomBall :: GlossWire [Event] Ball
randomBall = mkGen $ \ds events -> do
  c <- uniform [red, green, yellow, cyan, magenta, rose, azure, orange]
  -- Generate a position within the upper half of the screen
  halfWidth  <- (/2) <$> asks worldWidth
  halfHeight <- (/2) <$> asks worldHeight
  p <- getRandomR (Vec2 (-halfWidth) halfHeight, Vec2 halfWidth 0)
  stepWire (ball c p) ds (Right events)

ball :: Color -> Vec2 -> GlossWire [Event] Ball
ball c p0 = proc events -> do
  acc <- accelerate -< events
  rec (pos', collided) <- position p0 -< vel'
      vel' <- velocity -< (acc, collided)
  returnA -< Ball pos' c

accelerate :: GlossWire [Event] Vec2
accelerate = arr (gravityAcc &+) . userAccWire
  where
    gravityAcc  =  Vec2 0 (-500)
    userAccWire =  pure (mkVec2 (-2000, 0)) . when (isKeyDown KeyLeft)
               <|> pure (mkVec2 (2000, 0)) . when (isKeyDown KeyRight)
               <|> pure (mkVec2 (0, -2000)) . when (isKeyDown KeyDown)
               <|> pure (mkVec2 (0, 2000)) . when (isKeyDown KeyUp)
               <|> pure zero

velocity ::  GlossWire (Vec2, CollisionType) Vec2
velocity = integralVecWith bounce zero
  where 
    bounce HorizontalCollision (Vec2 x y) = clampVec $ mkVec2 (x * damp, y)
    bounce VerticalCollision (Vec2 x y) = clampVec $ mkVec2 (x, y * damp)
    bounce NoCollision v = v
    damp = -0.85
    clampVec vec@(Vec2 x y) | len vec < 50 = zero
                            | abs x < 50   = Vec2 0 y
                            | abs y < 50   = Vec2 x 0
                            | otherwise    = vec

position :: Vec2 -> GlossWire Vec2 (Vec2, CollisionType)
position = integralVecWithM' bounce
  where 
    bounce v = do
      width  <- asks worldWidth
      height <- asks worldHeight
      return $ bounce' (width / 2 - 10) (height / 2 - 10) v

    bounce' bWidth bHeight (Vec2 x y) = (Vec2 x' y', col)
      where
        x'  | x < (-bWidth) = (-x) - 2 * bWidth
            | x > bWidth = 2 * bWidth - x
            | otherwise = x

        y'  | y < (-bHeight) = (-y) - 2 * bHeight
            | y > bHeight = 2 * bHeight - y
            | otherwise = y

        col | x < (-bWidth)  || (x > bWidth)  = HorizontalCollision
            | y < (-bHeight) || (y > bHeight) = VerticalCollision
            | otherwise = NoCollision
            

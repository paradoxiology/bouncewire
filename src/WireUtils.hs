module WireUtils 
(
  integralVec,
  integralVecWith,
  integralVecWith',
  integralVecWithM',
  concatE,
  onThenE,
  accumWiresE,
  oneShot,
  periodicShots
)
where

import Control.Wire
import Control.Wire.Unsafe.Event

import Data.Vect
import Data.Fixed

import Prelude hiding ((.), id)


integralVec :: (HasTime t s, Vector v) => v -> Wire s e m v v
integralVec x0 = 
  mkPure $ \ds dx ->
    let dt = realToFrac (dtime ds)
    in x0 `seq` (Right x0, integralVec (x0 &+ dt *& dx)) 

integralVecWith :: (HasTime t s, Vector v) => (w -> v -> v) -> v -> Wire s e m (v, w) v
integralVecWith correct = go
  where 
    go x0 =
      mkPure $ \ds (dx, w) ->
        let dt = realToFrac (dtime ds)
            x1 = correct w (x0 &+ dt *& dx)
        in x0 `seq` (Right x0, go x1) 

integralVecWith' :: (HasTime t s, Vector v) => (v -> (v, o)) -> v -> Wire s e m v (v, o)
integralVecWith' correct = go
  where 
    go x0 =
      mkPure $ \ds dx ->
        let dt      = realToFrac (dtime ds)
            (x1, o) = correct (x0 &+ dt *& dx)
        in x0 `seq` (Right (x0, o), go x1) 

integralVecWithM' :: (HasTime t s, Monad m, Vector v)
                  => (v -> m (v, o))
                  -> v
                  -> Wire s e m v (v, o)
integralVecWithM' correct = go
  where 
    go x0 =
      mkGen $ \ds dx -> do
        let dt  =  realToFrac (dtime ds)
        -- Use res as opposed to explicitly destruct(pattern match)
        -- the tuple (x1, 0) to avoid evaluation of the tuple in the do notation
        -- (Because of the implicitly sequenced ordering)
        res <- correct (x0 &+ dt *& dx)
        return $ x0 `seq` (Right (x0, snd res), go (fst res)) 

concatE :: Event a -> Event a -> Event [a]
concatE NoEvent   NoEvent    = Event []
concatE (Event e) NoEvent    = Event [e]
concatE NoEvent   (Event e') = Event [e']
concatE (Event e) (Event e') = Event (e : e' : [])

onThenE :: (a -> Bool) -> b -> Wire s e m a (Event b)
onThenE f y = go
  where
    go = mkSFN $ \x -> if f x then (Event y, go) else (NoEvent, go)

accumWiresE :: (Monad m, Monoid s) => Wire s e m (a, Event [Wire s e m a b]) [b]
accumWiresE = go []
  where
    go wires = mkGen $ \ds (x, eWires) -> do
      let newWires = event [] id eWires
      stepped <- mapM (\w -> stepWire w ds (Right x)) (newWires ++ wires)
      -- Filter out inhibited wires, and extract the outputs and future wires out separately
      let (o', wires') = unzip [(o, w') | (Right o, w') <- stepped]
      return (Right o', go wires')

-- Produce one value now then inhibit
oneShot :: (Monoid e) => Wire s e m a a
oneShot = mkPureN $ \x -> (Right x, inhibit mempty)

-- Produce one value for one instance from now periodically
periodicShots :: (HasTime t s, Monoid e) => t -> Wire s e m a a
periodicShots int | int <= 0 =  error "periodicShots: Non-positive interval"
periodicShots int = mkPureN $ \x -> (Right x, go int)
  where
    go 0 = go int
    go t' =
        mkPure $ \ds x ->
            let t = t' - dtime ds
            in if t <= 0
                 then (Right x, go (mod' t int))
                 else (Left mempty, go t)

